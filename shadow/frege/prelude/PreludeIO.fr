{-
    Copyright © 2011, Ingo Wechsung
 
    All rights reserved.
 
    Redistribution and use in source and binary forms, with or
    without modification, are permitted provided that the following
    conditions are met:

    -   Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

    -   Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials provided
        with the distribution. Neither the name of the copyright holder
        nor the names of its contributors may be used to endorse or
        promote products derived from this software without specific
        prior written permission.
 
    *THIS SOFTWARE IS PROVIDED BY THE
    COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
    OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
    USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
    AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
    IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    THE POSSIBILITY OF SUCH DAMAGE.*
-}

{--
    This package provides classes and functions that deal with the lesser pure
    parts of Frege - Exceptions, Threads, mutable data.
    
    This package is /implementation specific/ insofar as the compiler may
    assume that certain items are defined here in a certain way.
    Changes may thus lead to compiler crashes or java code that 
    will be rejected by the java compiler.
    
    In particular, derived 'Exceptional' instances will reference
    type class 'Exceptional'.
    
 -}


protected package frege.prelude.PreludeIO where

import frege.prelude.PreludeBase
import frege.prelude.PreludeMonad


-- ########################## Exceptions ############################################

{-- 
    Make the @java.lang.Class@ object corresponding to the instantiated type available.
    
    Function 'catch' requires that the argument of a handler function
    is an instance of 'Exceptional'. 
    
    This is derivable for @pure@ @native@ data types.
    -}                                
    
class Exceptional e where
    --- The @java.lang.Class@ object of the instantiated type
    javaClass  :: Class e


--- nowarn: argument of type (ST s a)
--- Runtime method for implementation of 'catch'
protected native doCatch frege.runtime.WrappedCheckedException.doCatch 
                :: Class b -> ST s a -> (b -> ST s a) -> ST s a
{--
    The construct
    
    > action `catch` handler
    
    is a 'ST' action with the same type as _action_.
    
    If _action_ yields a result, this will be the result of the overall action.
    However, if, during execution of _action_ the JVM raises an exception _e_ with 
    java type @E@, and @E@ is a subtype of java type @H@, and @H@ is the 
    java type associated with the argument of _handler_, the return value will be:
    
    > handler e
    
    Otherwise, if the type of _e_ does not allow to pass it to _handler_ it will
    be propagated upwards and @a `catch` b@ will not return to its caller.
    
    Because 'catch' is left associative, it is possible to catch different exceptions,
    like in:
    
    > action `catch` handler1 `catch` handler2
    
    Care must be taken to check for the *most specific* exception first. In the example above,
    if the exception handled by _handler1_ is *less specific* than the one handled by _handler2_,
    then _handler2_ will never get a chance to execute. 
    
    Another way to put this is to say that if @E1@ and @E2@ are distinct exception types
    handled in a chain of 'catch'es, and @E1@ is (from the point of view of Java!) a
    subtype of @E2@, then the handler for @E1@ must appear further left than the handler for
    @E2@. If it is a super type of @E2@, however, its handler must appear further right.
    And finally, if the types do not stand in a sub-type relationship, the order of the 
    handlers is immaterial.
    
    *Note* If _action_ is of the form:
    
    > doSomething arg
    
    then, depending on the strictness of _doSomething_ the argument _arg_ may be evaluated
    *before* the action is returned. Exceptions (i.e. undefined values) 
    that occur in the construction of the action do *not* count as 
    exceptions thrown during execution of it, and hence cannot be catched.
    
    Example:
    
    > println (head []) `catch`  ....
    
    will not catch the exception that will be thrown when println evaluates  
    
    For a remedy, see 'try'.         
    -}
catch action handler = doCatch javaClass action handler

--- nowarn: argument of type (ST s a)
{--
    The construct
    
    > action `finally` always
    
    reurns the same value as _action_, when executed.
    
    However, no matter if _action_ produces a value or diverges 
    (for example, by throwing an exception), in any case will _always_ be executed,
    and its return value dismissed.
    
    Note that 'finally' only returns to its caller if _action_ would have done so.
    
    'finally' has the same fixity as 'catch', hence it is possible to have
    
    > action `catch` handler1 `catch` handler2 `finally` always
    
    -}
native finally frege.runtime.WrappedCheckedException.doFinally  :: IO a -> IO b -> IO a

--- Deliberatly throw an exception in the 'ST' monad.
native throwST frege.runtime.WrappedCheckedException.throwST :: PreludeBase.Throwable -> ST s ()

--- Deliberatly throw an exception in the 'IO' monad.
native throwIO frege.runtime.WrappedCheckedException.throwST :: PreludeBase.Throwable -> IO ()

{--
    Make sure that exceptions thrown during construction of an action can be catched.
    See 'catch' for an explanation.
    
    Example:
    
    > try println (head []) `catch` (\u::Undefined -> println u.catched)
    
    should print:
    
    > frege.runtime.Undefined: Prelude.head []
    
    'try' does work for unary functions only. 
    To be safe with functions taking more actions, use:
    
    > return a >>= (\a -> return b >>= (\b -> f a b))
    -}
try f a = return a >>= f
