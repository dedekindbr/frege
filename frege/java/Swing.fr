{- «•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»

    Copyright © 2011, Ingo Wechsung
    All rights reserved.

    Redistribution and use in source and binary forms, with or
    without modification, are permitted provided that the following
    conditions are met:

        Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        Redistributions in binary form must reproduce the above
        copyright notice, this list of conditions and the following
        disclaimer in the documentation and/or other materials provided
        with the distribution. Neither the name of the copyright holder
        nor the names of its contributors may be used to endorse or
        promote products derived from this software without specific
        prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE
    COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
    OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
    USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
    AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
    IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    THE POSSIBILITY OF SUCH DAMAGE.

    «•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•»«•» -}

{--
 * Definitions for native components below javax.swing
 -}


package frege.java.Swing where

import Java.Lang as JL()
import Java.Awt(Container, Component, ActionListenerT)
import Java.swing.GroupLayout as GL()

-- ------------------ SwingConstants ------------------------
{-- this is defined as an abstract type so as to enable
 * the access to the constants in the usual way.
 *
 * Note that the constants are in lowercase, in contast to java.
 -}
data SwingConstants = pure native javax.swing.SwingConstants where
    pure native bottom      javax.swing.SwingConstants.BOTTOM   :: Int
    pure native center      javax.swing.SwingConstants.CENTER   :: Int
    pure native east        javax.swing.SwingConstants.EAST     :: Int
    pure native horizontal  javax.swing.SwingConstants.HORIZONTAL :: Int
    pure native leading     javax.swing.SwingConstants.LEADING  :: Int
    pure native left        javax.swing.SwingConstants.LEFT     :: Int
    pure native next        javax.swing.SwingConstants.NEXT     :: Int
    pure native north       javax.swing.SwingConstants.NORTH    :: Int
    pure native north_east  javax.swing.SwingConstants.NORTH_EAST :: Int
    pure native north_west  javax.swing.SwingConstants.NORTH_WEST :: Int
    pure native previous    javax.swing.SwingConstants.PREVIOUS :: Int
    pure native right       javax.swing.SwingConstants.RIGHT    :: Int
    pure native south       javax.swing.SwingConstants.SOUTH    :: Int
    pure native south_east  javax.swing.SwingConstants.SOUTH_EAST :: Int
    pure native south_west  javax.swing.SwingConstants.SOUTH_WEST :: Int
    pure native top         javax.swing.SwingConstants.TOP      :: Int
    pure native trailing    javax.swing.SwingConstants.TRAILING :: Int
    pure native vertical    javax.swing.SwingConstants.VERTICAL :: Int
    pure native west        javax.swing.SwingConstants.WEST     :: Int


--- convenient name for type 'JFrameT'
type JFrame = JFrameT RealWorld
--- A @javax.swing.JFrame@
data JFrameT s = native javax.swing.JFrame where
    --- creates a new invisible frame with the specified title
    native new :: String -> IO JFrame
    --- creates a new invisible frame 
    native new0 new :: () -> IO JFrame
    --- Sets the operation that will happen by default when the user initiates a "close" on this frame.
    native setDefaultCloseOperation :: JFrame -> Int -> IO ()
    --- constant to dispose of the window when closed
    pure native dispose_on_close javax.swing.JFrame.DISPOSE_ON_CLOSE :: Int
    --- constant to do nothing when closed
    pure native do_nothing_on_close javax.swing.JFrame.DO_NOTHING_ON_CLOSE :: Int
    --- constant to exit the application when closed
    pure native exit_on_close javax.swing.JFrame.EXIT_ON_CLOSE :: Int
    --- constant to hide the window when closed
    pure native hide_on_close javax.swing.JFrame.HIDE_ON_CLOSE :: Int

    native getContentPane :: JFrame -> IO Container
    --- this really belongs to interface RootPaneConatiner
    native setContentPane :: JFrame -> Container -> IO ()
    

--- convenient name for 'JLabelT'
type JLabel = JLabelT RealWorld
--- a swing label
data JLabelT s = native javax.swing.JLabel where
    --- create a label
    native new :: String -> IO JLabel
    --- create a blank label
    native new0 new :: () -> IO JLabel
    --- Defines the single line of text this component will display. 
    native setText :: JLabelT s -> String -> ST s ()



--- wrapper for @javax.swing.SwingUtilities.invokeLater@
native invokeLater javax.swing.SwingUtilities.invokeLater :: JL.Runnable -> IO ()
--- wrapper for @javax.swing.SwingUtilities.invokeAndWait@
native invokeAndWait javax.swing.SwingUtilities.invokeAndWait :: JL.Runnable -> IO (Exception ())

-- ------------------- JComponent ----------------------------------

--- a 'JComponentT' in the 'IO' monad
type JComponent = JComponentT RealWorld
--- an object with compiler time type @javax.swing.JComponent@
data JComponentT s = native javax.swing.JComponent where
    native setOpaque :: JComponentT s -> Bool -> ST s ()


-- ---------------------------- JPanel ---------------------------------
--- a 'JPanelT' in the 'IO' monad
type JPanel = JPanelT RealWorld
--- an object with compiler time type @javax.swing.JPanel@
data JPanelT s = native javax.swing.JPanel where
    native new :: () -> ST s (JPanelT s)


-- ---------------------------- AbstractButton ---------------------------------


--- a 'AbstractButtonT' in the 'IO' monad
type AbstractButton = AbstractButtonT RealWorld
--- an object with compiler time type @javax.swing.AbstractButton@
data AbstractButtonT s = native javax.swing.AbstractButton where
    --- Sets the vertical position of the text relative to the icon.
    native setVerticalTextPosition :: AbstractButtonT s -> Int -> ST s ()
    --- Sets the horizontal position of the text relative to the icon.
    native setHorizontalTextPosition :: AbstractButtonT s -> Int -> ST s ()
    --- Adds an ActionListener to the button.
    native addActionListener :: AbstractButtonT s -> ActionListenerT s -> ST s ()
    --- set a text
    native setText :: AbstractButtonT s -> String -> ST s () 
    
    
-- ---------------------------- JButton ---------------------------------


--- a 'JButtonT' in the 'IO' monad
type JButton = JButtonT RealWorld
--- an object with compiler time type @javax.swing.JButton@
data JButtonT s = native javax.swing.JButton where
    --- create a button with label
    native new :: String -> ST s (JButtonT s)
    --- create a blank button
    native new0 new :: () -> ST s (JButtonT s)
-- ----------------------------- JTextField and JTextComponent -------------------------------

data JTextComponent s = native javax.swing.text.JTextComponent where
    native getText :: JTextComponent s -> ST s String
    native setText :: JTextComponent s -> String -> ST s ()
    
--- a 'JTextFieldT' in the 'IO' monad
type JTextField = JTextFieldT RealWorld
--- an object with compile time type @javax.swing.JTextField@
data JTextFieldT s = native javax.swing.JTextField where
    native new :: () -> ST s (JTextFieldT s)

-- ----------------------------- GroupLayout ---------------------------------------

type GroupLayout = GroupLayoutT RealWorld
data GroupLayoutT s = native javax.swing.GroupLayout where
    native new :: Container -> IO GroupLayout
    native setHorizontalGroup :: GroupLayout -> GL.Group -> IO ()     