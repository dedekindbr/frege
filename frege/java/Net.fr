{--
    Java types from java.net
-}
package frege.java.Net where

-- import Java.Lang

{--
    Frege type for @java.net.URL@, note that we do not support operations
    that mutate the URL, hence we regard every instance as 'Immutable'
-}
data URLT a = native java.net.URL where
    native new              :: String -> IO (CatchAll URL)
    native new2 new         :: URL -> String -> IO (CatchAll URL)
    native openStream       :: URL -> IO (CatchAll InputStream)
    pure native toString    :: URL -> String
    
--- The normal form of an URL -- 'Immutable'
type URL        = URLT Immutable
--- The normal form of an InputStream -- 'RealWorld' (mutated by IO actions) 
protected type InputStream     = InputStreamT RealWorld
protected data InputStreamT a  = native java.io.InputStream 
  
type URLArray = URLArrayT Immutable
instance Cloneable URLArrayT where
    native our frege.runtime.Runtime.our :: URLArrayT s -> ST s URLArray

data URLArrayT s = native "java.net.URL[]" where
    native new    "java.net.URL[]" :: Int -> ST s (URLArrayT s)
    native getAt  "frege.runtime.Array.<java.net.URL>arrayGet"
                                                :: URLArrayT s -> Int -> ST s (Maybe URL)
    native setAt  "frege.runtime.Array.<java.net.URL>arraySet"
                                                :: URLArrayT s -> Int -> URL -> ST s ()
    pure native itemAt "frege.runtime.Array.<java.net.URL>arrayGet"
                                                :: URLArrayT Immutable -> Int -> Maybe URL
    --- use this only if it is absolutely sure that there are no nulls in the array
    pure native elemAt "frege.runtime.Array.<java.net.URL>arrayGet"
                                                :: URLArrayT Immutable -> Int -> URL
    pure native length "frege.runtime.Array.<java.net.URL>arrayLen"
                                                :: URLArrayT a -> Int
    toList (a::URLArray) = elems a 0
        where
            elems (a::URLArray) i
                | i < a.length = case itemAt a i of
                    Just s  -> s:elems a (i+1)
                    Nothing -> elems a (i+1)
                | otherwise = []
    fromListST :: [URL] -> ST u URLArray
    fromListST urls = (URLArray.new urls.length >>= loop 0 urls) >>= our where
        loop j (x:xs) arr = do URLArray.setAt arr j x; loop (j+1) xs arr
        loop j []     arr = return arr
    fromList urls = ST.run (fromListST urls)


data URI = pure native java.net.URI where
    pure native new                        :: String -> CatchAll URI
    pure native create java.net.URI.create :: String -> URI
    pure native toURL                      :: URI -> CatchAll URL
    pure native toString                   :: URI -> String
    pure native toASCIIString              :: URI -> String
    pure native relativize                 :: URI -> URI -> URI



type URLClassLoader = URLClassLoaderT RealWorld
data URLClassLoaderT σ = native java.net.URLClassLoader where
        native new             :: URLArray -> ClassLoader -> IO (CatchAll URLClassLoader)
        native loadClass       :: URLClassLoader -> String -> IO (CatchAll(Class a))
        native getResource     :: URLClassLoader -> String -> IO (Maybe URL)
        native findResource    :: URLClassLoader -> String -> IO (Maybe URL)



      