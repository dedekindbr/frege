{--
    Classes from java.io
-}

package frege.java.IO where

--- The normal form of an InputStream -- 'RealWorld' (mutated by IO actions)
type InputStream = InputStreamT RealWorld

--- The normal form of a Closeable -- 'RealWorld' (mutated by IO actions)
type Closeable   = CloseableT   RealWorld

--- frege equivalent of @java.io.InputStream@
data InputStreamT s = native java.io.InputStream where
    native read  :: InputStream -> IO (Exception Int)
    
--- frege equivalent of @java.io.Closeable@    
data CloseableT s = native java.io.Closeable where    
    native close :: Closeable -> IO (Exception ())


protected data URI = pure native java.net.URI

type File = FileT RealWorld
data FileT s = native java.io.File where
    native new                :: String -> Mutable FileT s
    native newFS     new      :: FileT s -> String -> Mutable FileT s

    pure native toURI         :: Frozen FileT -> URI
    pure native getPathF  getPath    :: Frozen FileT -> String
    pure native isAbsolute    :: Frozen FileT -> Bool
    pure native separator        java.io.File.separator
                              :: String
    pure native pathSeparator    java.io.File.pathSeparator
                              :: String

    native getPath       :: File -> IO String
    native getName       :: File -> IO String
    native canRead       :: File -> IO Bool
    native canWrite      :: File -> IO Bool
    native isDirectory   :: File -> IO Bool
    native isFile        :: File -> IO Bool
    native exists        :: File -> IO Bool
    native mkdirs        :: File -> IO Bool
    native delete        :: File -> IO Bool
    native renameTo      :: File -> File -> IO Bool
    native lastModified  :: File -> IO Long
    native getParentFile :: File -> IO (Maybe File)
    native list          :: File -> IO (Maybe StringArr)

instance Serializable FileT

instance Show (FileT Immutable) where show = File.getPathF

type Writer = WriterS RealWorld
data WriterS s = native java.io.Writer

type PrintWriter = PrintWriterS RealWorld
data PrintWriterS s = native java.io.PrintWriter where
    native print    :: PrintWriter -> String -> IO ()
    native println  :: PrintWriter -> String -> IO ()
    native printLn  println :: PrintWriter -> IO ()
    native open new :: String -> IO (Exception PrintWriter)
    native new      :: File -> IO (Exception PrintWriter)
    native encoded new :: File -> String -> IO (Exception PrintWriter)
    native fromWriter   new    :: Writer -> IO PrintWriter
    native fromWriterAf new    :: Writer -> Bool -> IO PrintWriter
    
--- a 'StringWriter' suitable for the 'IO' 'Monad'.
type StringWriter = StringWriterS RealWorld

{-- 
    Frege type for a @java.io.StringWriter@
    
    Not intended for direct use but rather as something
    a 'PrintWriter' can be made of. (Though, because
    of the 'Appendable' inheritance, one could 
    still 'append' directly.)
    
    To be used like:
    
    > action :: PrintWriter -> IO ()
    > action =  ...
    > actionOnStringWriter :: IO String  
    > actionOnStringWriter = do
    >       sw <- StringWriter.new
    >       pr <- sw.printer
    >       action pr
    >       pr.close
    >       sw.toString  
    -}    
data StringWriterS s = native java.io.StringWriter where
    --- create a fresh 'StringWriter'
    native new      :: () -> ST s (StringWriterS s)
    --- get the content of a 'StringWriter' as 'String'    
    native toString :: StringWriterS s -> ST s String
    --- make a 'PrintWriter' that prints to this 'StringWriter'
    native printer new :: StringWriterS s -> IO PrintWriter
            